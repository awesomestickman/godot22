# WorldComplete.gd
extends Area2D

export(String, FILE, "*.tscn") var next_world

func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player":
			if CharacterSelection.characterSelection == "D'Arion":
				CharacterSelection._set_character("Charles")
			elif CharacterSelection.characterSelection == "Charles":
				CharacterSelection._set_character("D'Arion")
			get_tree().change_scene(next_world)
