extends Control

func _on_StoryNext_pressed():
	if $CharlesStoryImg.animation == "Story1":
		$CharlesStoryImg.play("Story2")
	elif $CharlesStoryImg.animation == "Story2":
		$CharlesStoryImg.play("Story3")
	elif $CharlesStoryImg.animation == "Story3":
		$CharlesStoryImg.play("Story4")
	elif $CharlesStoryImg.animation == "Story4":
		$CharlesStoryImg.play("Story5")
	elif $CharlesStoryImg.animation == "Story5":
		get_tree().change_scene("res://Scenes/TheEnd.tscn")
