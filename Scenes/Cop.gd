extends KinematicBody2D

#load the projectile
const TearGas = preload("res://Scenes/teargas.tscn")
#how often throw teargas
const THROWTIMER = 200
#keep track of held item
var held_item = null

#keep track of where teargas spawns
onready var held_item_position = get_node("police/Position2D")

#get location of player
onready var playerpos = get_node("../Player")

var timer = 0;


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

#make the rock
func spawnGas():
	if held_item==null:
		held_item = TearGas.instance()
		held_item_position.add_child(held_item)
	
func _throw_held_item():
	#direction which way to throw it
	var launch = -1
	$police.flip_h=false
	if playerpos.get_global_position().x > self.get_global_position().x:
		$police.flip_h=true
		launch = 1
	held_item.launch(launch)
	held_item=null

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timer-=1
	if(timer<150):
		$police.play("stand")
	if(timer<50):
		$police.play("wind")
	if(timer<0):
		$police.play("throw")
		spawnGas()
		_throw_held_item()
		
		timer=THROWTIMER
	
