extends Control

signal health_changed(health)

func _ready():
	var health_node = null
	for node in get_tree().get_nodes_in_group("actors"):
		if node.name == "Player":
			health_node = node.get_node("Health")
			break
	$Bars/LifeBar.initialize(health_node.max_health)
	
	if CharacterSelection.characterSelection == "D'Arion":
		$PortraitNode/HealthPortrait.play("D'ArionHealth")
		$PortraitNode/Name.text = "D'Arion"
	elif CharacterSelection.characterSelection == "Charles":
		$PortraitNode/HealthPortrait.play("CharlesHealth")
		$PortraitNode/Name.text = "Charles"

func _on_Health_health_changed(health):
	emit_signal("health_changed", health)
