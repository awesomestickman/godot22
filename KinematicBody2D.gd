extends KinematicBody2D

const UP = Vector2(0, -1)
const GRAVITY = 20
const ACCELERATION = 50
const MAX_SPEED = 250

const JUMP_HEIGHT = -550
#invincibility frames
const HEALTHTIMER = 20

#for use in collision checking with drop thru platforms
#make sure when adding tileset for fall through - add the SCENE as a node
const DROP_THRU_BIT = 1

var healthframes = 1

var motion = Vector2()

func _physics_process(delta):
	healthframes-=1
	motion.y += GRAVITY
	var friction = false
	
	# Move Right
	if Input.is_action_pressed("ui_right"):
		motion.x = min(motion.x+ACCELERATION, MAX_SPEED)
		$Sprite.flip_h = false
		if CharacterSelection.characterSelection == "D'Arion":
			$Sprite.play("D'Arion_Run")
		elif CharacterSelection.characterSelection == "Charles":
			$Sprite.play("Charles_Run")
	
	# Move Left
	elif Input.is_action_pressed("ui_left"):
		motion.x = max(motion.x-ACCELERATION, -MAX_SPEED)
		$Sprite.flip_h = true
		if CharacterSelection.characterSelection == "D'Arion":
			$Sprite.play("D'Arion_Run")
		elif CharacterSelection.characterSelection == "Charles":
			$Sprite.play("Charles_Run")
	
	# Idle
	else:
		if CharacterSelection.characterSelection == "D'Arion":
			$Sprite.play("D'Arion_Idle")
		elif CharacterSelection.characterSelection == "Charles":
			$Sprite.play("Charles_Idle")
		friction = true
	
	if is_on_floor():
		# Jump
		if Input.is_action_just_pressed("ui_up"):
			motion.y = JUMP_HEIGHT
		# If holding down, allow player to drop through 
		if Input.is_action_pressed(("ui_down")):
			# Disable the 1 bit to not check for drop thru platform collisions
			set_collision_mask_bit(DROP_THRU_BIT,false)
		
		if friction == true:
			motion.x = lerp(motion.x, 0, 0.2)
	else:
		# Jump Animation
		if motion.y < 0:
			if CharacterSelection.characterSelection == "D'Arion":
				$Sprite.play("D'Arion_Jump")
			elif CharacterSelection.characterSelection == "Charles":
				$Sprite.play("Charles_Jump")
		else:
		# Fall Animation
			if CharacterSelection.characterSelection == "D'Arion":
				$Sprite.play("D'Arion_Fall")
			elif CharacterSelection.characterSelection == "Charles":
				$Sprite.play("Charles_Fall")
		if friction == true:
			motion.x = lerp(motion.x, 0, 0.05)
	
	motion = move_and_slide(motion, UP)
	pass

#using signals have have linked this event from fallthroughassistant 
#to the player code
func _on_FallThroughAssistant_body_exited(body):
	#renable the 1 bit to scan for drop through platform collisions
	set_collision_mask_bit(DROP_THRU_BIT,true)

#same thing as above, just hacking it together
func _on_FallThroughAssistant_body_entered(body):
	set_collision_mask_bit(DROP_THRU_BIT,true)

func takeDamage():
	if(healthframes<0):
		print_debug("take damage")
		healthframes=HEALTHTIMER
		$Health.take_damage(1)
		if($Health.health == 0):
			get_tree().reload_current_scene()

func _input(event):
	# Damage button ("X") to test health code and animation
	if event.is_action_pressed("damage"):
		$Health.take_damage(1)
	# Switch button ("Z") to test switching between character animations
	if event.is_action_pressed("switch"):
		if CharacterSelection.characterSelection == "D'Arion":
			CharacterSelection._set_character("Charles")
		elif CharacterSelection.characterSelection == "Charles":
			CharacterSelection._set_character("D'Arion")
