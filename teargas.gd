extends KinematicBody2D

const THROW_VELOCITY = Vector2(200,-200)

#load the projectile
const TearGas = preload("res://Scenes/Particles2D.tscn")
#keep track of teargas
var held_item = null
#keep track of where teargas spawns
onready var held_item_position = get_node("Position2D")

var velocity = Vector2.ZERO
var spinning = true
var rng = RandomNumberGenerator.new()

func _ready():
	#get current scene
	
	pass
	
func launch(direction):
	#save it's initiation posistion
	var temp = global_transform
	#make it so it's movement not relative to parent
	var current_scene = get_tree().get_current_scene()
	get_parent().remove_child(self)
	current_scene.add_child(self)
	#give that posistion back so it's not put at 0,0 (which happens when add to scene)
	global_transform = temp
	rng.randomize()
	velocity = Vector2(THROW_VELOCITY.x*direction, THROW_VELOCITY.y*rng.randf_range(0.5,2))
#
	print_debug(velocity)
	

func _physics_process(delta):
	#spin2win
	if(spinning):
		
		get_node("AnimatedSprite").set_rotation(get_node("AnimatedSprite").get_rotation()+rng.randf_range(5,10))
	#maybe set gravity to something
	#velocity.y += ProjectSettings.gravity*delta
	velocity.y += 500*delta
	#created if collides with something
	var collision = move_and_collide(velocity*delta)
	if collision != null:
		$AnimatedSprite.play("land")
		if spinning:
			held_item = TearGas.instance()
			held_item_position.add_child(held_item)
			spinning = false
		#call function for handling impact and bouncing
		_on_impact(collision.normal)

func _on_impact(normal : Vector2):
	velocity = velocity.bounce(normal)
	velocity *= 0.5 + rand_range(-0.05,0.05)


func _on_Area2D_body_entered(body):
	body.takeDamage()
