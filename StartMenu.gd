#StartMenu.gd
extends Control

func _on_StartGameButton_pressed():
	get_tree().change_scene("res://Scenes/CharacterSelect.tscn")

func _on_CreditsButton_pressed():
	get_tree().change_scene("res://Scenes/Credits.tscn")

func _on_ExitGameButton_pressed():
	get_tree().quit()
